# -*- coding: utf-8 -*-
import argparse
import urllib,bisect,os,ipaddress

__author__  = "Lukasz Wojton"
__copyright__ = "Copyright 2010, Lukasz Wojton"
__license__ = "GPL"
__version__ = "1.0.1"
__email__ = "lukasz@wojton.eu"

class MaskCalc:

    def __init__(self):
        masks = {}
        m_start = 30
        m_count = 4
        while (m_start > 0):
            masks[m_count] = str(m_start)
            m_start = m_start - 1
            m_count = m_count * 2

        self.masks = masks

    def get_mask(self, ip_count):
        try:
            return self.masks[ip_count]
        except KeyError:
            return False

    def get_optymalized_mask(self, ip_count):
        masks = sorted(self.masks.keys())
        index = bisect.bisect(masks, ip_count)

        try:
            count = masks[index-1]
        except KeyError:
            return False

        mask = self.get_mask(count)
        return {"mask": mask, "ip_count":count}


if __name__ == '__main__':
    parser = argparse.ArgumentParser("python ripencc-by-country.py")
    parser.add_argument("-r", help="Refresh file with networks from ripe", action="store_true")
    parser.add_argument("-c", help="Coutry code default is PL", default="PL")
    parser.add_argument("-f", help="File path to save the list", default="")
    args = parser.parse_args()  # type: map

    remote_link = 'ftp://ftp.ripe.net/pub/stats/ripencc/delegated-ripencc-latest'
    local_file = '/tmp/delegated-ripencc-latest'

    country = args.c
    if os.path.isfile(local_file) == False or args.r == True:
        urllib.urlretrieve(remote_link, local_file)

    file = open(local_file, "r")
    file_data = file.readlines()
    maskCalc = MaskCalc()

    addr_to_split = []
    network_list = []
    for line in file_data:
        if line.find(country)==-1 or line.find("ipv4")==-1:
            continue

        data = line.split("|")

        network_ip = data[3] # kolumna z adresem sieci
        ip_count = int(data[4]) # kolumna z liczba adresow

        mask = maskCalc.get_mask(ip_count)
        if (mask != False):
            network_list.append("%s/%s" % (network_ip, mask))
        else:
            addr_to_split.append({'netaddr': network_ip, "ip_count": ip_count})

    for addr in addr_to_split:
        network_ip = unicode(addr['netaddr'])
        ip_count = addr['ip_count']
        while ip_count>0:
            near_mask = maskCalc.get_optymalized_mask(ip_count)
            mask = near_mask['mask']

            ip_count = ip_count - near_mask['ip_count']
            network_list.append("%s/%s" % (network_ip, mask))
            network_ip = unicode(ipaddress.ip_address(network_ip) + near_mask['ip_count'])

    if args.f == '':
        for network_ip in network_list:
            print network_ip
    else:
        file = open(args.f, 'w')
        for network_ip in network_list:
            file.write(network_ip+"\n")

        file.close()









